export interface ServiceConfiguration {
  authorizationEndpoint: string;
  tokenEndpoint: string;
  revocationEndpoint?: string;
  registrationEndpoint?: string;
}

export type BaseConfiguration =
  | {
      issuer?: string;
      serviceConfiguration: ServiceConfiguration;
    }
  | {
      issuer: string;
      serviceConfiguration?: ServiceConfiguration;
    };

type CustomHeaders = {
  authorize?: Record<string, string>;
  token?: Record<string, string>;
  register?: Record<string, string>;
};

type AdditionalHeaders = Record<string, string>;

interface BuiltInParameters {
  display?: 'page' | 'popup' | 'touch' | 'wap';
  login_prompt?: string;
  prompt?: 'consent' | 'login' | 'none' | 'select_account';
}

export type BaseAuthConfiguration = BaseConfiguration & {
  clientId: string;
};

export type AuthConfiguration = BaseAuthConfiguration & {
  scopes: string[];
  redirectUrl: string;
  env?: 'DEV' | 'SAND' | 'PROD';
};

export interface AuthorizeResult {
  accessToken: string;
  accessTokenExpirationDate: string;
  tokenAdditionalParameters?: { [name: string]: string };
  refreshToken: string;
  tokenType: string;
  scopes: string[];
}

export interface RefreshResult {
  accessToken: string;
  accessTokenExpirationDate: string;
  additionalParameters?: { [name: string]: string };
  refreshToken: string | null;
  tokenType: string;
}



export interface RefreshConfiguration {
  refreshToken: string;
}

export function prefetchConfiguration(config: AuthConfiguration): Promise<void>;


export function authorize(config: AuthConfiguration): Promise<AuthorizeResult>;

export function refresh(
  config: AuthConfiguration,
  refreshConfig: RefreshConfiguration
): Promise<RefreshResult>;

// https://tools.ietf.org/html/rfc6749#section-4.1.2.1
type OAuthAuthorizationErrorCode =
  | 'unauthorized_client'
  | 'access_denied'
  | 'unsupported_response_type'
  | 'invalid_scope'
  | 'server_error'
  | 'temporarily_unavailable';
// https://tools.ietf.org/html/rfc6749#section-5.2
type OAuthTokenErrorCode =
  | 'invalid_request'
  | 'invalid_client'
  | 'invalid_grant'
  | 'unauthorized_client'
  | 'unsupported_grant_type'
  | 'invalid_scope';
type AppAuthErrorCode =
  | 'service_configuration_fetch_error'
  | 'authentication_failed'
  | 'token_refresh_failed'
  | 'registration_failed'
  | 'browser_not_found';

type ErrorCode =
  | OAuthAuthorizationErrorCode
  | OAuthTokenErrorCode
  | AppAuthErrorCode;

export interface AppAuthError extends Error {
  code: ErrorCode;
}
