import invariant from 'invariant';
import { NativeModules, Platform } from 'react-native';

const { RNAppAuth } = NativeModules;

const ISSUER_DEV = 'https://web-components.dev.identyum.com/authorize';
const SERVICE_CONFIGURATION_DEV = {
  authorizationEndpoint: "https://web-components.dev.identyum.com/authorize",
  tokenEndpoint: "https://identifier.dev.identyum.com/api/v1/auth/token"
};

const ISSUER_SAND = 'https://web-components.stage.identyum.com/authorize';
const SERVICE_CONFIGURATION_SAND = {
  authorizationEndpoint: "https://web-components.stage.identyum.com/authorize",
  tokenEndpoint: "https://identifier.stage.identyum.com/api/v1/auth/token"
};


const ISSUER_PROD = 'https://web-components.live.identyum.com/authorize';
const SERVICE_CONFIGURATION_PROD = {
  authorizationEndpoint: "https://web-components.live.identyum.com/authorize",
  tokenEndpoint: "https://identifier.live.identyum.com/api/v1/auth/token"
};


const validateClientId = clientId =>
  invariant(typeof clientId === 'string', 'Config error: clientId must be a string');
const validateRedirectUrl = redirectUrl =>
  invariant(typeof redirectUrl === 'string', 'Config error: redirectUrl must be a string');

const validateHeaders = headers => {
  if (!headers) {
    return;
  }
  const customHeaderTypeErrorMessage =
    'Config error: customHeaders type must be { token?: { [key: string]: string }, authorize?: { [key: string]: string }, register: { [key: string]: string }}';

  const authorizedKeys = ['token', 'authorize', 'register'];
  const keys = Object.keys(headers);
  const correctKeys = keys.filter(key => authorizedKeys.includes(key));
  invariant(
    keys.length <= authorizedKeys.length &&
    correctKeys.length > 0 &&
    correctKeys.length === keys.length,
    customHeaderTypeErrorMessage
  );

  Object.values(headers).forEach(value => {
    invariant(typeof value === 'object', customHeaderTypeErrorMessage);
    invariant(
      Object.values(value).filter(key => typeof key !== 'string').length === 0,
      customHeaderTypeErrorMessage
    );
  });
};

const validateAdditionalHeaders = headers => {
  if (!headers) {
    return;
  }

  const errorMessage = 'Config error: additionalHeaders must be { [key: string]: string }';

  invariant(typeof headers === 'object', errorMessage);
  invariant(
    Object.values(headers).filter(key => typeof key !== 'string').length === 0,
    errorMessage
  );
};

export const prefetchConfiguration = async ({
  redirectUrl,
  clientId,
  scopes,
  env = 'SAND'

}) => {

  const dangerouslyAllowInsecureHttpRequests = false;
  const customHeaders = undefined;
  const warmAndPrefetchChrome = true;

  const ISSUER = env === 'DEV' ? ISSUER_DEV : (env === 'PROD' ? ISSUER_PROD : ISSUER_SAND);
  const SERVICE_CONFIGURATION = env === 'DEV' ? SERVICE_CONFIGURATION_DEV : (env === 'PROD' ? SERVICE_CONFIGURATION_PROD : SERVICE_CONFIGURATION_SAND);


  if (Platform.OS === 'android') {
    validateClientId(clientId);
    validateRedirectUrl(redirectUrl);
    validateHeaders(customHeaders);

    const nativeMethodArguments = [
      warmAndPrefetchChrome,
      ISSUER,
      redirectUrl,
      clientId,
      scopes,
      SERVICE_CONFIGURATION,
      dangerouslyAllowInsecureHttpRequests,
      customHeaders,
    ];

    RNAppAuth.prefetchConfiguration(...nativeMethodArguments);
  }
};

export const authorize = ({
  redirectUrl,
  clientId,
  scopes,
  env = 'SAND'
}) => {

  const usePKCE = false;
  const useNonce = true;
  const clientAuthMethod = 'basic';
  const clientSecret = undefined;
  const additionalParameters = undefined;
  const dangerouslyAllowInsecureHttpRequests = false;
  const customHeaders = undefined;
  const additionalHeaders = undefined;
  const skipCodeExchange = false;


  const ISSUER = env === 'DEV' ? ISSUER_DEV : (env === 'PROD' ? ISSUER_PROD : ISSUER_SAND);
  const SERVICE_CONFIGURATION = env === 'DEV' ? SERVICE_CONFIGURATION_DEV : (env === 'PROD' ? SERVICE_CONFIGURATION_PROD : SERVICE_CONFIGURATION_SAND);


  validateClientId(clientId);
  validateRedirectUrl(redirectUrl);
  validateHeaders(customHeaders);
  validateAdditionalHeaders(additionalHeaders);

  const nativeMethodArguments = [
    ISSUER,
    redirectUrl,
    clientId,
    clientSecret,
    scopes,
    additionalParameters,
    SERVICE_CONFIGURATION,
    skipCodeExchange,
  ];

  if (Platform.OS === 'android') {
    nativeMethodArguments.push(usePKCE);
    nativeMethodArguments.push(clientAuthMethod);
    nativeMethodArguments.push(dangerouslyAllowInsecureHttpRequests);
    nativeMethodArguments.push(customHeaders);
  }

  if (Platform.OS === 'ios') {
    nativeMethodArguments.push(additionalHeaders);
    nativeMethodArguments.push(useNonce);
    nativeMethodArguments.push(usePKCE);
  }

  return RNAppAuth.authorize(...nativeMethodArguments);
};

export const refresh = (
  {
    redirectUrl,
    clientId,
    scopes,
    env = 'SAND'
  },
  { refreshToken }
) => {

  const clientAuthMethod = 'basic';
  const clientSecret = undefined;
  const additionalParameters = undefined;
  const dangerouslyAllowInsecureHttpRequests = false;
  const customHeaders = undefined;
  const additionalHeaders = undefined;

  const ISSUER = env === 'DEV' ? ISSUER_DEV : (env === 'PROD' ? ISSUER_PROD : ISSUER_SAND);
  const SERVICE_CONFIGURATION = env === 'DEV' ? SERVICE_CONFIGURATION_DEV : (env === 'PROD' ? SERVICE_CONFIGURATION_PROD : SERVICE_CONFIGURATION_SAND);

  validateClientId(clientId);
  validateRedirectUrl(redirectUrl);
  validateHeaders(customHeaders);
  validateAdditionalHeaders(additionalHeaders);
  invariant(refreshToken, 'Please pass in a refresh token');
  // TODO: validateAdditionalParameters

  const nativeMethodArguments = [
    ISSUER,
    redirectUrl,
    clientId,
    clientSecret,
    refreshToken,
    scopes,
    additionalParameters,
    SERVICE_CONFIGURATION,
  ];

  if (Platform.OS === 'android') {
    nativeMethodArguments.push(clientAuthMethod);
    nativeMethodArguments.push(dangerouslyAllowInsecureHttpRequests);
    nativeMethodArguments.push(customHeaders);
  }

  if (Platform.OS === 'ios') {
    nativeMethodArguments.push(additionalHeaders);
  }

  return RNAppAuth.refresh(...nativeMethodArguments);
};